from flask import Flask, render_template, url_for
app = Flask(__name__)

@app.route('/')     
@app.route('/home')
@app.route('/index.html')
def home():
    return render_template('index.html', title='Home')

@app.route('/about')
@app.route('/about.html')
def about():
    return render_template('about.html', title="About")

if __name__ == '__main__':
    app.run(host="0.0.0.0",port=8080,debug=True)
